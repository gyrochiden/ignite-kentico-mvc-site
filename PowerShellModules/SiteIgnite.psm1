function Install-Kentico {
    param(
        [string] $SolutionPath,
        [string] $IgnitionPath,
        [string] $UtilsPath
    )

    begin {
        Set-Location $SolutionPath
        Import-Module -Name SqlServer -Force
        $admin_solution_built = $FALSE
        $mvc_solution_built = $FALSE
    }

    process {
        Write-Host "" 
        Write-Host "Installing Admin Site" -ForegroundColor Cyan
        $admin_solution_built = Install-Admin-Site -SolutionPath $SolutionPath -IgnitionPath $IgnitionPath -UtilsPath $UtilsPath
        Write-Host "" 
        Write-Host "Installing MVC Site" -ForegroundColor Cyan
        $mvc_solution_built = Install-MVC-Site -SolutionPath $SolutionPath -IgnitionPath $IgnitionPath -UtilsPath $UtilsPath
    }

    end {
        Write-Host("")
        Write-Host("Site Ignition Completed!") -ForegroundColor Green
        Write-Host("")
        if(!($admin_solution_built)) {            
            Write-Host("WARNING: Admin site will not work until the solution has been built") -ForegroundColor Yellow
            Write-Host("you need to open the solution file and perform a build!") -ForegroundColor Yellow
            Write-Host("")
            Write-Host("WARNING: CI data has not been restored, you need to run a CI data restore manually") -ForegroundColor Yellow
            Write-Host("")
        }
        if(!($mvc_solution_built)) {            
            Write-Host("WARNING: MVC site will not work until the solution has been built") -ForegroundColor Yellow
            Write-Host("you need to open the solution file and perform a build!") -ForegroundColor Yellow
            Write-Host("")
        }
        if(!($admin_solution_built) -or !($mvc_solution_built)) {            
            Write-Host("NOTE: Some versions of Visual Studio struggle with package restore for Roslyn") -ForegroundColor Magenta
            Write-Host("the best way around this is to build the solution, and then close Visual Studio") -ForegroundColor Magenta
            Write-Host("then re-open the solution and run a 'Build > Clean', then rebuild the solution") -ForegroundColor Magenta
            Write-Host("subsequent builds should work as normal!") -ForegroundColor Magenta
            Write-Host("")
        }
        Write-Host("If this site is on a shared drive you may need to update IIS to point to the proper UNC path!") -ForegroundColor Cyan
        Write-Host("")        
        Write-Host("You may now close this window.....") -ForegroundColor Green
    }

}

function Install-Admin-Site {
    param(
        [string] $SolutionPath,
        [string] $IgnitionPath,
        [string] $UtilsPath
    )

    begin {
        Set-Location $SolutionPath
        Import-Module -Name SqlServer -Force
        $config = (Get-Content .\ignition.json) | ConvertFrom-Json
        $admin_solution_built = $FALSE
    }

    process {

            if($config.cms.createDatabase) {
                Get-Development-Database $config
                Install-Database $config
            }

            Update-IISSettings $config.cms
            Update-FolderPermissions $config.cms          
            Update-HostFile $config.cms

            
            $build_yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Builds the solution"
            $build_no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Continues without building the solution"
            $build_options = [System.Management.Automation.Host.ChoiceDescription[]]($build_yes, $build_no)
            $build_result = Show-Menu 'Build Solution?' 'Would you like to attempt to build the admin site solution?' $build_options

            switch ($build_result)
            {
                0 {
                    try {
                        Invoke-MSBuild $config.cms.solutionFile
                        $admin_solution_built = $TRUE
                    } catch {
                        Write-Warning("Building Solution Failed!!")
                    }
                }
            }

            if($admin_solution_built) {
                Restore-CIData -SolutionPath $SolutionPath -IgnitionPath $IgnitionPath
            }

            $admin_solution_built
    }
}

function Install-MVC-Site {
    param(
    [string] $SolutionPath,
    [string] $IgnitionPath,
    [string] $UtilsPath
    )

    begin {
        Set-Location $SolutionPath
        Import-Module -Name SqlServer -Force
        $config = (Get-Content .\ignition.json) | ConvertFrom-Json
        $mvc_solution_built = $FALSE
    }

    process {
            Update-IISSettings $config.site 
            Update-FolderPermissions $config.site
            Update-HostFile $config.site

            
            $build_yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Builds the solution"
            $build_no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Continues without building the solution"
            $build_options = [System.Management.Automation.Host.ChoiceDescription[]]($build_yes, $build_no)
            $build_result = Show-Menu 'Build Solution?' 'Would you like to attempt to build the MVC solution?' $build_options

            switch ($build_result)
            {
                0 {
                    try {
                        Invoke-MSBuild $config.site.solutionFile
                        $mvc_solution_built = $TRUE
                    } catch {
                        Write-Warning("Building Solution Failed!!")
                    }
                }
            }

            $mvc_solution_built
    }
}

function Get-Development-Database {
    #Download Base DB File
    Write-Host "Downloading the base database image..."
    $url = $config.cms.baseDBUrl
    $bacPac = $config.cms.bacpacFile
    $db_output_path = "$IgnitionPath\DB\$bacPac"
    (New-Object System.Net.WebClient).DownloadFile($url, $db_output_path)
}

function Install-Database {

    begin {
        Import-Module $IgnitionPath\PowerShellModules\SQLLocalDBUtils.psm1
        $SQL_SERVER = $config.cms.databaseServer + $config.cms.databaseInstance
        $SQL_CONNECTION_STRING = "Data Source=$SQL_SERVER;Initial Catalog=master;Connection Timeout=0;Integrated Security=True;"
        $BACPAC_FILE = Join-Path "$IgnitionPath\DB\" $config.cms.bacpacFile
        $DAC_FX_PATH = Join-Path $UtilsPath "\Microsoft.SqlServer.DacFx.x64\lib\net46\Microsoft.SqlServer.Dac.dll"
        $dbUsername = $config.cms.databaseUser
        $dbPassword = $config.cms.databasePassword
        $database = $config.cms.databaseName
    }

    process {
        Write-Host Creating SQL localdb instance $config.cms.databaseInstance
        Add-LocalDb($config.cms.databaseInstance) | Out-Null

        if ((Test-Path $DAC_FX_PATH) -eq $false) {
            Throw 'No usable version of DacFx found.'
        }
        else {
            try {
                Add-Type -Path $DAC_FX_PATH
            }
            catch {
                Throw 'No usable version of DacFx found.'
            }
        }

        $bacPacRestored = $FALSE
        try {
            Write-Host Importing $config.cms.bacpacFile to $SQL_SERVER
            $DacServices = New-Object Microsoft.SqlServer.Dac.DacServices $SQL_CONNECTION_STRING
            $BacPackage = [Microsoft.SqlServer.Dac.BacPackage]::Load($BACPAC_FILE)
            $DacServices.ImportBacpac($BacPackage, $database)
            $bacPacRestored = $TRUE
        } catch {
            #Write-Warning "Database Import Failed! Database has probably already been imported. If you need to re-import the initial database it must be dropped using SQL Management Studio first."
        }

        if(!($bacPacRestored)) {
            $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Drops the database and restores the base database."
            $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Keeps the current database and continues."
            $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
            $result = Show-Menu "Database $database already exists!" 'Do you wish to delete the existing database and restore the base database?' $options

            switch ($result)
            {
                0 {
                    Write-Host("Dropping $database database...")
                    $drop_sql="
                        USE [master]
                        GO

                        DROP DATABASE [$database]
                        GO"

                    Invoke-Sqlcmd -Server $SQL_SERVER -Query $drop_sql | Out-Null
                    try {
                        Write-Host Importing $config.cms.bacpacFile to $SQL_SERVER
                        $DacServices = New-Object Microsoft.SqlServer.Dac.DacServices $SQL_CONNECTION_STRING
                        $BacPackage = [Microsoft.SqlServer.Dac.BacPackage]::Load($BACPAC_FILE)
                        $DacServices.ImportBacpac($BacPackage, $database)
                        $bacPacRestored = $TRUE
                    } catch {
                        Write-Warning "Database DROP and re-import Failed! If you need to re-import the initial database it must be dropped/deleted using SQL Management Studio first."
                    }

                }
                1 { Write-Host "Existing database has been retained"}
            }
        }

        Write-Host("Fixing login for user $dbUsername")
        $sql="
        USE [master]
        GO

        if not exists (select loginname from master.dbo.syslogins where loginname='$dbUsername')
            CREATE LOGIN [$dbUsername] WITH PASSWORD='$dbPassword'
        GO

        ALTER LOGIN [$dbUsername] WITH PASSWORD = '$dbPassword';
        GO

        use [$database]
        GO

        IF EXISTS (SELECT name FROM [sys].[database_principals] WHERE  name = N'$dbUsername')
            DROP USER [$dbUsername]
        GO

        CREATE USER [$dbUsername] FOR LOGIN [$dbUsername] WITH DEFAULT_SCHEMA=[dbo]
        GO

        ALTER ROLE [db_owner] ADD MEMBER [$dbUsername]
        GO"

        Invoke-Sqlcmd -Server $SQL_SERVER -Query $sql | Out-Null
        Enable-LocalDbSharing($config.cms.databaseInstance) | Out-Null
        Stop-LocalDb($config.cms.databaseInstance) | Out-Null
        Start-LocalDb($config.cms.databaseInstance) | Out-Null

        Write-Host("Extending auto-close/shutdown timeout for localdb instance")
        $autoShutdownExtendSQL="
        sp_configure 'show advanced options', 1;
        RECONFIGURE;
        GO
        sp_configure 'user instance timeout', 65535;
        GO"

        Invoke-Sqlcmd -Server $SQL_SERVER -Query $autoShutdownExtendSQL | Out-Null
        Stop-LocalDb($config.cms.databaseInstance) | Out-Null
        Start-LocalDb($config.cms.databaseInstance) | Out-Null
    }

    end {

        Write-Host Initial Database Setup Complete!

    }

}

function Update-FolderPermissions {
    param(
        [PSCustomObject] $cfg
    )

    begin { 
        Write-Host "Updating folder permissions for IIS"
        $siteName = $cfg.siteName
        $appPoolSid = (Get-ItemProperty IIS:\AppPools\$siteName).applicationPoolSid
        $identifier = New-Object System.Security.Principal.SecurityIdentifier $appPoolSid
        $appPoolUser = $identifier.Translate([System.Security.Principal.NTAccount])
        $iisSiteRoot = $cfg.iisSiteRoot
    }

	process {
        $Acl = Get-Acl .\$iisSiteRoot
		$permissions = "IIS_IUSRS", 'Read,Modify', 'ContainerInherit,ObjectInherit', 'None', 'Allow'
        $permissions_anon = "IUSR", 'Read', 'ContainerInherit,ObjectInherit', 'None', 'Allow'
        $permissions_apppool = $appPoolUser, 'FullControl', 'ContainerInherit,ObjectInherit', 'None', 'Allow'
		$rule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $permissions
        $rule_apppool = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $permissions_apppool
        $rule_anon = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $permissions_anon        
        $Acl.SetAccessRule($rule)
        $Acl.SetAccessRule($rule_apppool)
		$Acl.SetAccessRule($rule_anon)
		Set-Acl .\$iisSiteRoot $Acl
    }

	end { Write-Host "Updated folder permissions" }
}

function Update-IISSettings {

    param(
        [PSCustomObject] $cfg
    )

    begin{
        Import-Module webadministration
        $siteName = $cfg.siteName
        $bindings = $cfg.bindings
		$logicalDisk = (Get-WmiObject Win32_LogicalDisk -filter "DriveType = 4 AND DeviceID = '$((Get-Location).Drive.Name):'").ProviderName
        $siteRoot = $cfg.iisSiteRoot
        $iisPath =  "$SolutionPath\$siteRoot"
		if($logicalDisk) {
			$uncPath = (Get-Location).Path.Replace((Split-Path -qualifier (Get-Location).Path), $logicalDisk)
			$iisPath = "$uncPath\$siteRoot"
		}
		
    }

    process {
        Write-Host "Setting up IIS..."
		if((Test-Path "IIS:\Sites\Default Web Site")) {
		   Set-ItemProperty "IIS:\Sites\Default Web Site" -name bindings -value @{protocol="http";bindingInformation="*:80:localhost"}
		}

        Write-Host Creating IIS App Pool... [$siteName]
		if((Test-Path IIS:\AppPools\$siteName)) {
		    Remove-Item IIS:\AppPools\$siteName -Recurse -Force	
		}
        New-Item IIS:\AppPools\$siteName | Write-Verbose

		Write-Host "Creating IIS Site... [$siteName]"
        if((Test-Path IIS:\Sites\$siteName)) {
            Remove-Item IIS:\Sites\$siteName -Recurse -Force
        }

		New-Item IIS:\Sites\$siteName -bindings @{protocol="http";bindingInformation="*:80:$($bindings[0])"} -physicalPath $iisPath -Force | Write-Verbose
		Set-ItemProperty IIS:\Sites\$siteName -name applicationPool -value $siteName
		Write-Host "    " adding binding for $bindings[0]        

        if($bindings.Length -gt 1) {
		    For ($i=1; $i -lt $bindings.Length; $i++) {
			    Write-Host "    " adding binding for $bindings[$i]
			    New-WebBinding $siteName -IPAddress "*" -Port 80 -HostHeader $($bindings[$i])			  
		    }
        }
    }

}

function Update-HostFile {

    param(
        [PSCustomObject] $cfg
    )

    begin {
        $bindings = $cfg.bindings
    }

    process {
        Write-Host "Setting up host file entries..."
        . (Join-Path $PSScriptRoot HostFileUtils.ps1) add 127.0.0.1 $bindings[0]
        Write-Host "    " adding host file entry for $bindings[0]

        if($bindings.Length -gt 1) {
		    For ($i=1; $i -lt $bindings.Length; $i++) {
                Write-Host "    " adding host file entry for $bindings[$i]
			   . (Join-Path $PSScriptRoot HostFileUtils.ps1) add 127.0.0.1 $bindings[$i]
		    }
        }
    }

    end {}
}

function Restore-CIData {
    param(
    [string] $SolutionPath,
    [string] $IgnitionPath
    )

    begin {
        Import-Module $IgnitionPath\PowerShellModules\KenticoCI.psm1 -Force
    }

    process {

        $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Restores the Kentico CI data from disk"
        $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Continues without restoring CI data"
        $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
        $result = Show-Menu 'Restore CI Data?' 'Would you like to restore Kentico CI data?' $options

        switch ($result)
        {
            0 {
                try {
                    Restore-KenticoCIData -SolutionPath $SolutionPath -IgnitionPath $IgnitionPath
                } catch {
                    Write-Warning("Kentico CI Restore failed!!")
                }
            }
        }

        
    }

    end {}
}

function Get-MSBuildPath ([switch] $amd64) {
    Write-Host "Finding MSBuild...."
    $vsInstallPath = (Get-VsSetupInstance).InstallationPath    
    $msBuildPaths = Get-ChildItem -Path $vsInstallPath -Recurse | Where-Object { $_.Name -ieq 'MsBuild.exe' }   
    $msBuildPathsSorted = $msBuildPaths | Sort-Object -Property FullName -Descending

    $msBuildPath = $null 
    if ($amd64)
    {
        $msBuildPath = $msBuildPathsSorted | Where-Object { $_.Directory.Name -ieq 'amd64' } | Select-Object -ExpandProperty FullName -First 1
    }
    else
    {
        $msBuildPath = $msBuildPathsSorted | Where-Object { $_.Directory.Name -ine 'amd64' } | Select-Object -ExpandProperty FullName -First 1        
    }

    $msBuildPath
}

function Invoke-MSBuild ($solution, $target = "Rebuild", [switch] $amd64) {
    Write-Host ""
    Write-Host "Building Solution..."
    # allow for wildcard matching on $solution
    $solutionMatches = @(Get-ChildItem $solution -ErrorAction SilentlyContinue)
 
    if ($solutionMatches.Length -ne 1) {
        Write-Warning "Expected 1 solution match but found $($solutionMatches.Length). Refine solution path."
        return
    }
 
    $sln = $solutionMatches[0]
    $msBuildPath = Get-MSBuildPath -amd64:$amd64
    
    $UTILS_DIR = ("$env:LOCALAPPDATA\.gyro\igniteSiteUtils\")
    $NUGET = Join-Path $UTILS_DIR "nuget.exe"

    Write-Host "Restoring packages for $($sln.FullName)"
    Invoke-Expression "&`"$NUGET`" restore `"$($sln.FullName)`" -Verbosity quiet"
       
    Write-Host "Running target $($target) for $($sln.FullName)"
    &$msBuildPath -v:q -m $sln.FullName /t:$target  
}

function Show-Menu {
    param(
        [string] $title,
        [string] $message,
        [System.Management.Automation.Host.ChoiceDescription[]] $options
    )

    $result = $host.ui.PromptForChoice($title, $message, $options, 0)

    $result
}

Export-ModuleMember -function Install-Kentico