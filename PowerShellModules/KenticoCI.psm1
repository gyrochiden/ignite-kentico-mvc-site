function Restore-KenticoCIData {
    param (
        [string] $SolutionPath,
        [string] $IgnitionPath
    )
    
    begin {
        Set-Location $SolutionPath
        $_CFG = (Get-Content .\ignition.json) | ConvertFrom-Json 
        $ADMIN_DIR = Join-Path $SolutionPath $($_CFG.cms).iisSiteRoot
        $CI_TOOL = Join-Path $ADMIN_DIR "\bin\ContinuousIntegration.exe"
    }


    process {
        Set-Variable -Name "dbInstanceName" -Value $($_CFG.cms).databaseInstance
        Import-Module $IgnitionPath\PowerShellModules\SQLLocalDBUtils.psm1 -Force
        Write-Host "Ensuring (localdb)\($dbInstanceName) is started"
        Start-LocalDb($dbInstanceName) | Out-Null
        Write-Host "Running Kentico CI Process..."
        Invoke-Expression "&`"$CI_TOOL`" -r"
    }
}

Export-ModuleMember -function Restore-KenticoCIData