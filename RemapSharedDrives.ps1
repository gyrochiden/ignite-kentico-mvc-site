[cmdletbinding()]
param(
    [string] $IgnitionPath = ""
)
$commandContent =  (Get-Content $IgnitionPath\PowerShellModules\DriveRemapper.ps1) | Out-String
$command = "& { $commandContent }"
$bytes = [System.Text.Encoding]::Unicode.GetBytes($command)
$encodedCommand = [Convert]::ToBase64String($bytes)
PowerShell.exe -NoProfile -Command "& {Start-Process PowerShell.exe -ArgumentList '-ExecutionPolicy Bypass -encodedCommand $encodedCommand' -Verb RunAs}"